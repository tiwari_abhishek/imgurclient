package dev.abhishek.imgurassist.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.model.Uploads;

public class MyUploadsAdapter extends RecyclerView.Adapter<MyUploadsAdapter.ViewHolder> {

    private static final String IMG_URL = "https://i.imgur.com/";
    private Context context;
    private List<Uploads> uploadsList;

    public MyUploadsAdapter(Context context, List<Uploads> uploadsList) {
        this.context = context;
        this.uploadsList = uploadsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout_gallery_tag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.pin_title.setText(uploadsList.get(position).getName());
        Glide.with(context).load(IMG_URL+uploadsList.get(position).getId()+".png")
                .apply(new RequestOptions().placeholder(R.drawable.placeholder)
                .fitCenter())
                .into(holder.pin_image);

        final TextView message = new TextView(context);
        final FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = context.getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.gravity = Gravity.CENTER;

        final SpannableString s = new SpannableString(IMG_URL+uploadsList.get(position).getId());
        Linkify.addLinks(s, Linkify.WEB_URLS);
        message.setLayoutParams(params);
        message.setText(s);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        container.addView(message);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(container)
                        .setCancelable(true);
                AlertDialog info = builder.create();
                info.setTitle("LINK TO IMGUR");
                info.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return uploadsList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder{
        private TextView pin_title;
        private ImageView pin_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pin_image = itemView.findViewById(R.id.pin_image);
            pin_title = itemView.findViewById(R.id.pin_title);
        }
    }
}
