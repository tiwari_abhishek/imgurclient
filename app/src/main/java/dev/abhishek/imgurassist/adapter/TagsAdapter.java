package dev.abhishek.imgurassist.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.model.Tag;
import dev.abhishek.imgurassist.view.GalleryTagActivity;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {

    private Context context;
    private List<Tag> tagList;

    public TagsAdapter(Context context, List<Tag> tagList) {
        this.context = context;
        this.tagList = tagList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout_tags_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tv_tagName.setText(tagList.get(position).getDisplayName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryTagActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("tagname", tagList.get(position).getName());
                intent.putExtra("tagDisplayName", tagList.get(position).getDisplayName());
                context.getApplicationContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tagList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_tagName;
        private ImageView ic_tag;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_tagName = itemView.findViewById(R.id.tv_tagName);
            ic_tag = itemView.findViewById(R.id.ic_tag);
        }
    }
}
