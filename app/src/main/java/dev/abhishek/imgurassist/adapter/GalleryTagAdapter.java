package dev.abhishek.imgurassist.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.model.Image;
import dev.abhishek.imgurassist.view.fragments.ImageBottomSheetFragment;

public class GalleryTagAdapter extends RecyclerView.Adapter<GalleryTagAdapter.ViewHolder> {

    private static final String IMG_URL = "https://i.imgur.com/";
    private Context context;
    private List<Image> imageList;
    private ImageBottomSheetFragment bottomSheetFragment;
    private FragmentManager bottomSheetBehaviour;

    public GalleryTagAdapter(Context context, List<Image> imageList, FragmentManager fragmentManager) {
        this.context = context;
        this.imageList = imageList;
        this.bottomSheetBehaviour = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout_gallery_tag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.pin_title.setText(imageList.get(position).getTitle());

        final String img_url = IMG_URL+imageList.get(position).getCover()+".png";
        Glide.with(context)
                .load(img_url)
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder)
                .fitCenter())
                .into(holder.pin_image);

        bottomSheetFragment = new ImageBottomSheetFragment();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bottomSheetFragment.isAdded()){
                    Bundle bundle = new Bundle();
                    bundle.putString("title", imageList.get(position).getTitle());
                    bundle.putString("desc", imageList.get(position).getDescription());
                    bundle.putString("timestamp", String.valueOf(imageList.get(position).getDatetime()));
                    bundle.putString("username", imageList.get(position).getAccountUrl().toString());
                    bundle.putString("url", img_url);
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(bottomSheetBehaviour,"Image Details");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView pin_image;
        private TextView pin_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pin_image = itemView.findViewById(R.id.pin_image);
            pin_title = itemView.findViewById(R.id.pin_title);
        }
    }
}
