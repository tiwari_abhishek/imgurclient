package dev.abhishek.imgurassist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.model.Contact;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private Context context;
    private List<Contact> contactList;
    private FragmentManager fragmentManager;

    public ContactListAdapter(Context context, List<Contact> contactList, FragmentManager fragmentManager) {
        this.context = context;
        this.contactList = contactList;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.rl_contact.setAnimation(AnimationUtils.loadAnimation(context,R.anim.recycler_view_anim));
        holder.tv_name.setText(contactList.get(position).getName());
        holder.tv_phnum.setText(contactList.get(position).getNumber());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name;
        private TextView tv_phnum;
        private RelativeLayout rl_contact;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_contactName);
            tv_phnum = itemView.findViewById(R.id.tv_phnum);
            rl_contact = itemView.findViewById(R.id.rl_contact);
        }
    }
}
