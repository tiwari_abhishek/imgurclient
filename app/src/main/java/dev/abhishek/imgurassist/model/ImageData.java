package dev.abhishek.imgurassist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageData {
    @Expose
    private Image data;
    @SerializedName("data")

    public Image getData() {
        return data;
    }

    public void setData(Image data) {
        this.data = data;
    }
}
