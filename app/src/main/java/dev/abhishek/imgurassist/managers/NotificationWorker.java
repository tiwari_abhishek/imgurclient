package dev.abhishek.imgurassist.managers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.model.Contact;
import dev.abhishek.imgurassist.view.MainActivity;
import dev.abhishek.imgurassist.view.fragments.ContactListFragment;


public class NotificationWorker extends Worker {
    private static final String WORK_RESULT = "work_result";
    private Context context;
    private static final int BUFFER = 80000;

    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }
    @NonNull
    @Override
    public Result doWork() {
        Data taskData = getInputData();
        String taskDataString = taskData.getString(MainActivity.MESSAGE_STATUS);
        showNotification("Work Manager", taskDataString != null ? taskDataString : "Zipping contact files...");
        File myExternalFile = new File(context.getExternalFilesDir("MyFiles"), "test.csv");
        List<Contact> contacts = ContactListFragment.contactList;
        String data="Name,Phone Numeber\n";
        for(int i=0;i<contacts.size(); i++){
            data+=contacts.get(i).getName()+","+contacts.get(i).getNumber()+"\n";
        }
        try {
            myExternalFile = new File(context.getExternalFilesDir("MyFiles"), "test.csv");
            FileOutputStream fos = new FileOutputStream(myExternalFile);
            fos.write(data.getBytes());
            Log.e("wrie", "afA");
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] _files = new String[1];
        File zipFile = new File(context.getExternalFilesDir("MyFiles"), "contacts.zip");
        String zipFileName = zipFile.getAbsolutePath();
        _files[0] = myExternalFile.getAbsolutePath();
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte buffer_data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(buffer_data, 0, BUFFER)) != -1) {
                    out.write(buffer_data, 0, count);
                }
                origin.close();
            }
            Log.e("ZIPP", "done");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Data outputData = new Data.Builder().putString(WORK_RESULT, "Jobs Finished").build();
        return Result.success(outputData);
    }
    public void showNotification(String task, String desc) {
        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "task_channel";
        String channelName = "task_name";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new
                    NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle(task)
                .setContentText(desc)
                .setSmallIcon(R.mipmap.ic_launcher);
        manager.notify(1, builder.build());
    }
}