package dev.abhishek.imgurassist.managers;

import android.app.Activity;
import android.content.Intent;

import dev.abhishek.imgurassist.R;

public class ActivitySwitchManager {

    Activity activity;
    Class NewActivity;
    Intent mMenuIntent;

    public ActivitySwitchManager(Activity activity, Class NewActivity){

        this.activity = activity;
        this.NewActivity = NewActivity;
         mMenuIntent = new Intent(activity,
                NewActivity);
    }

    public void openActivity(int direction){


        mMenuIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(mMenuIntent);
        activity.finish();
        if(direction==1){
            activity.overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        }else if(direction==-1){
            activity.overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        }

    }

    public void openActivityWithoutSlide(){


        mMenuIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(mMenuIntent);
        activity.finish();

    }
    public void openActivityWthoutFinish(){


        activity.startActivity(mMenuIntent);
        activity.overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);

    }

}
