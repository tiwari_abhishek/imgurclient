package dev.abhishek.imgurassist.managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.model.Contact;
import dev.abhishek.imgurassist.model.Uploads;

public class DatabaseHandlers extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "db_uploads";
    private static final String TABLE_CONTACTS = "myuploads";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESC = "description";

    public DatabaseHandlers(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE myuploads (id TEXT, name TEXT, description TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new contact
    public void addRecord(Uploads uploads) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, uploads.getId());
        values.put(KEY_NAME, uploads.getName());
        values.put(KEY_DESC, uploads.getDesc());

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        Log.e("INSERT","SUCCESS");
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get all contacts in a list view
    public List<Uploads> getAllRecords() {
        List<Uploads> uploadsList = new ArrayList<Uploads>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Uploads uploads = new Uploads(cursor.getString(0), cursor.getString(1), cursor.getString(2));
                // Adding contact to list
                uploadsList.add(uploads);
            } while (cursor.moveToNext());
        }

        // return contact list
        return uploadsList;
    }

    public void deleteAll(){
        String selectQuery = "DELETE FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        db.rawQuery(selectQuery, null);
    }
    // Getting contacts Count
    public int getUploadsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
}
