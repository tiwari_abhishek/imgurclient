package dev.abhishek.imgurassist.view;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.managers.DatabaseHandlers;
import dev.abhishek.imgurassist.model.ImageResponse;
import dev.abhishek.imgurassist.model.TagsRoot;
import dev.abhishek.imgurassist.model.UploadedImage;
import dev.abhishek.imgurassist.model.Uploads;
import dev.abhishek.imgurassist.services.ApiClient;
import dev.abhishek.imgurassist.services.PostImageService;
import dev.abhishek.imgurassist.services.TagsService;
import dev.abhishek.imgurassist.utils.DocumentHelper;
import dev.abhishek.imgurassist.utils.NotificationHelper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static dev.abhishek.imgurassist.utils.Constants.PICK_IMAGE_REQUEST;
import static dev.abhishek.imgurassist.utils.Constants.READ_WRITE_EXTERNAL;

public class UploadImageActivity extends AppCompatActivity implements View.OnClickListener{

    private File chosenFile;
    private Uri returnUri;
    private RelativeLayout rl_add_camera;
    private RelativeLayout rl_add_computer;
    private RelativeLayout rl_details;
    private Button btn_upload;
    private TextInputEditText etv_name;
    private TextInputEditText etv_desc;
    private ImageView selectedImage;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;
    private DatabaseHandlers databaseHandlers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);

        databaseHandlers = new DatabaseHandlers(this);

        etv_name = findViewById(R.id.name);
        etv_desc = findViewById(R.id.description);
        selectedImage = findViewById(R.id.selected_image);
        rl_add_camera = findViewById(R.id.rl_add_camera);
        rl_add_computer = findViewById(R.id.rl_add_computer);
        rl_details = findViewById(R.id.rl_details);
        btn_upload = findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(this);
        rl_add_camera.setOnClickListener(this);
        rl_add_computer.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.rl_add_camera){
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            }
            else
            {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
        if(view.getId()==R.id.rl_add_computer){
            onChoose(view);
        }
        if(view.getId()==R.id.btn_upload){
            onUpload(view);
        }
    }

    public void onChoose(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }


    public void onUpload(View v) {
        if (chosenFile == null) {
            Toast.makeText(UploadImageActivity.this, "Choose a file before upload.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }else{
            final NotificationHelper notificationHelper = new NotificationHelper(this.getApplicationContext());
            notificationHelper.createUploadingNotification();


            PostImageService service = ApiClient.getRetrofitInstance().create(PostImageService.class);
            final Call<ImageResponse> call = service.postImage(
                    etv_name.getText().toString(),
                    etv_desc.getText().toString(),"","",
                    MultipartBody.Part.createFormData(
                            "image",
                            chosenFile.getName(),
                            RequestBody.create(MediaType.parse("image/*"), chosenFile))
            );
            call.enqueue(new Callback<ImageResponse>() {
                @Override
                public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                    if (response == null) {
                        Log.e("NULL","returned");
                        notificationHelper.createFailedUploadNotification();
                        return;
                    }
                    if (response.isSuccessful()) {
                        Toast.makeText(UploadImageActivity.this, "Upload successful !", Toast.LENGTH_SHORT)
                                .show();
                        Log.d("URL Picture", "http://imgur.com/" + response.body().data.id);
                        databaseHandlers.addRecord(new Uploads(response.body().data.id,etv_name.getText().toString(), etv_desc.getText().toString()));
                        notificationHelper.createUploadedNotification(response.body());
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ImageResponse> call, Throwable t) {
                    Toast.makeText(UploadImageActivity.this, "An unknown error has occured.", Toast.LENGTH_SHORT)
                            .show();
                    notificationHelper.createFailedUploadNotification();
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            rl_details.setVisibility(View.VISIBLE);
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            selectedImage.setImageBitmap(photo);
            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(getApplicationContext(), photo);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            chosenFile = new File(getRealPathFromURI(tempUri));
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            returnUri = data.getData();
            Log.e("URI", returnUri.toString());
            try {
                rl_details.setVisibility(View.VISIBLE);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), returnUri);
                selectedImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            super.onActivityResult(requestCode, resultCode, data);
            Log.d(this.getLocalClassName(), "Before check");

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                final List<String> permissionsList = new ArrayList<String>();
                addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE);
                addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (!permissionsList.isEmpty())
                    ActivityCompat.requestPermissions(UploadImageActivity.this,
                            permissionsList.toArray(new String[permissionsList.size()]),
                            READ_WRITE_EXTERNAL);
                else
                    getFilePath();
            } else {
                getFilePath();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private void getFilePath() {
        String filePath = DocumentHelper.getPath(this, this.returnUri);
        //Safety check to prevent null pointer exception
        if (filePath == null || filePath.isEmpty()) return;
        chosenFile = new File(filePath);
        Log.e("FilePath", filePath);
        Log.e("FilePath", chosenFile.getAbsolutePath());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            shouldShowRequestPermissionRationale(permission);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_EXTERNAL:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(UploadImageActivity.this, "All Permission are granted.", Toast.LENGTH_SHORT)
                            .show();
                    getFilePath();
                } else {
                    Toast.makeText(UploadImageActivity.this, "Some permissions are denied", Toast.LENGTH_SHORT)
                            .show();
                }
                return;
            }

            case MY_CAMERA_PERMISSION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
                else
                {
                    Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
