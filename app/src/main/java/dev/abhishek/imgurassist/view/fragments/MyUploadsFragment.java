package dev.abhishek.imgurassist.view.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.adapter.MyUploadsAdapter;
import dev.abhishek.imgurassist.managers.DatabaseHandlers;
import dev.abhishek.imgurassist.model.Uploads;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyUploadsFragment extends Fragment {

    private Context context;
    private DatabaseHandlers db;
    private List<Uploads> uploadsList;
    private RecyclerView rv_myUploads;
    private MyUploadsAdapter adapter;
    public MyUploadsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_uploads, container, false);

        db = new DatabaseHandlers(view.getContext());
        context = view.getContext();
        uploadsList = db.getAllRecords();
        rv_myUploads = view.findViewById(R.id.rv_myUploads);
        for(Uploads upload : uploadsList){
            Log.e("upload:", upload.getId()+" ");
        }
        setUpRecyclerView();
        return view;
    }

    public void setUpRecyclerView(){
        rv_myUploads.setLayoutManager(new StaggeredGridLayoutManager(2,1));
        adapter = new MyUploadsAdapter(context, uploadsList);
        rv_myUploads.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
