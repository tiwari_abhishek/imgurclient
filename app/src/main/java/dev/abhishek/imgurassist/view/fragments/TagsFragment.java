package dev.abhishek.imgurassist.view.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.adapter.TagsAdapter;
import dev.abhishek.imgurassist.model.Tag;
import dev.abhishek.imgurassist.model.TagsRoot;
import dev.abhishek.imgurassist.services.ApiClient;
import dev.abhishek.imgurassist.services.TagsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagsFragment extends Fragment {

    private Context context;
    private List<Tag> tagsList;
    private RecyclerView rv_tagsList;
    private TagsAdapter adapter;
    private NestedScrollView tags_progress_bar;
    private FragmentManager childFragmentManager;

    public TagsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tagsfragment, container, false);
        context = view.getContext();
        childFragmentManager = getChildFragmentManager();
        rv_tagsList = view.findViewById(R.id.rv_tagsList);
        tags_progress_bar = view.findViewById(R.id.tags_progress_bar);
        tags_progress_bar.setVisibility(View.VISIBLE);
        getTagsList();
        return view;
    }

    private void setUpRecyclerView(){
        rv_tagsList.setLayoutManager(new GridLayoutManager(context, 2));
        rv_tagsList.setHasFixedSize(true);
        adapter = new TagsAdapter(context,tagsList);
        rv_tagsList.setAdapter(adapter);
        tags_progress_bar.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();

    }
    private void getTagsList(){
        tagsList = new ArrayList<>();
        TagsService service = ApiClient.getRetrofitInstance().create(TagsService.class);
        Call<TagsRoot> call = service.getTags();
        call.enqueue(new Callback<TagsRoot>() {
            @Override
            public void onResponse(Call<TagsRoot> call, Response<TagsRoot> response) {
                tagsList = response.body().getTagsData().getTags();
                Log.e("RESPONSE", response.body().getStatus().toString());
                setUpRecyclerView();
            }

            @Override
            public void onFailure(Call<TagsRoot> call, Throwable t) {
                Log.e("RESPONSE", t.getMessage()+"djfbadf");
            }
        });
    }
}
