package dev.abhishek.imgurassist.view.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import dev.abhishek.imgurassist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageBottomSheetFragment extends BottomSheetDialogFragment {

    private Context context;
    private Toolbar toolbar;
    private TextView post_title;
    private TextView post_username;
    private TextView post_time;
    private TextView post_desc;
    private ImageView post_img;
    private ImageView toolbar_back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image_bottom_sheet, container, false);
        context = view.getContext();


        Bundle bundle = getArguments();
        String desc = bundle.getString("desc");
        String username = bundle.getString("username");
        String time = bundle.getString("datetime");
        toolbar = view.findViewById(R.id.toolbar);
        toolbar_back = view.findViewById(R.id.toolbar_back);
        post_title = view.findViewById(R.id.post_title);
        post_username = view.findViewById(R.id.post_username);
        post_time = view.findViewById(R.id.post_time);
        post_desc = view.findViewById(R.id.post_desc);
        post_img = view.findViewById(R.id.post_img);
        post_title.setText(bundle.getString("title"));

        if(desc!=null){
            post_desc.setText(bundle.getString("desc"));
        }else{
            post_desc.setText("No description available");
        }
        if(username!=null){
            post_username.setText(username);
        }else{
            post_username.setText("");
        }

        Glide.with(view.getContext()).load(bundle.getString("url")).apply(new RequestOptions().fitCenter()).into(post_img);

        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }


    @NonNull
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });
        return  dialog;
    }
    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
