package dev.abhishek.imgurassist.view.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.adapter.ContactListAdapter;
import dev.abhishek.imgurassist.managers.NotificationWorker;
import dev.abhishek.imgurassist.model.Contact;
import dev.abhishek.imgurassist.view.MainActivity;

public class ContactListFragment extends Fragment{

    private NestedScrollView progress_bar;
    private static final String TAG = "Contact info";
    public Context context;
    View view;

    private RecyclerView rv_contact;
    private ContactListAdapter adapter;
    public static List<Contact> contactList;
    public static HashMap<String, String> directory = new HashMap<>();
    private Button btn_gen;
    private TextView tv_status_csv;
    private FragmentManager childFragmentManager;

    public ContactListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        context = view.getContext();

        childFragmentManager = getChildFragmentManager();
        //define contents
        tv_status_csv = view.findViewById(R.id.tv_status_csv);
        progress_bar = view.findViewById(R.id.progress_bar);
        rv_contact = view.findViewById(R.id.rv_contactList);
        btn_gen = view.findViewById(R.id.btn_gen);
        btn_gen.setEnabled(false);

        progress_bar.setVisibility(View.VISIBLE);
        if(contactList==null){
            new FetchContacts(context.getApplicationContext(), new FetchContacts.OnContactFetchListener() {
                @Override
                public void onContactFetch(List<Contact> list) {
                    Collections.sort(contactList, new Comparator<Contact>() {
                        public int compare(Contact c1, Contact c2) {
                            // notice the cast to (Integer) to invoke compareTo
                            return (c1.getName()).compareTo(c2.getName());
                        }
                    });
                    progress_bar.setVisibility(View.GONE);
                    rv_contact.setLayoutManager(new LinearLayoutManager(context));
                    adapter = new ContactListAdapter(context, contactList, childFragmentManager);
                    rv_contact.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    btn_gen.setEnabled(true);
                }
            }).execute();
        }else{
            Collections.sort(contactList, new Comparator<Contact>() {
                public int compare(Contact c1, Contact c2) {
                    // notice the cast to (Integer) to invoke compareTo
                    return (c1.getName()).compareTo(c2.getName());
                }
            });
            btn_gen.setEnabled(true);
            progress_bar.setVisibility(View.GONE);
            rv_contact.setLayoutManager(new LinearLayoutManager(context));
            adapter = new ContactListAdapter(context, contactList, childFragmentManager);
            rv_contact.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        btn_gen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkWriteExternalPermission() && isExternalStorageAvailable() && !isExternalStorageReadOnly()){
                    final WorkManager mWorkManager = WorkManager.getInstance();
                    final OneTimeWorkRequest mRequest = new OneTimeWorkRequest.Builder(NotificationWorker.class).build();
                    mWorkManager.enqueue(mRequest);
                    mWorkManager.getWorkInfoByIdLiveData(mRequest.getId()).observe((LifecycleOwner) context, new Observer<WorkInfo>() {
                        @Override
                        public void onChanged(@Nullable WorkInfo workInfo) {
                            if (workInfo != null) {
                                WorkInfo.State state = workInfo.getState();
                                if(state.isFinished()){
                                    File myExternalFile = new File(context.getExternalFilesDir("MyFiles"), "contacts.zip");
                                    tv_status_csv.setText("Contacts stored at "+myExternalFile.getAbsolutePath());
                                    tv_status_csv.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });
                }else{
                    Toast.makeText(context, "Permission not available", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }

    private boolean checkWriteExternalPermission()
    {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
    public static class FetchContacts extends AsyncTask<Void, Void, List<Contact>> {
        private Context activity;
        private OnContactFetchListener listener;
        public FetchContacts(Context context, OnContactFetchListener listener) {
            activity = context;
            this.listener = listener;
        }
        @Override
        protected List<Contact> doInBackground(Void... params) {
            Log.e("CONTACT FETCH", "STARTED");
            contactList = new ArrayList<>();
            ContentResolver cr = activity.getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);

            if ((cur != null ? cur.getCount() : 0) > 0) {
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));

                    if (cur.getInt(cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        while (pCur.moveToNext()) {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if(!directory.containsKey(phoneNo)){
                                contactList.add(new Contact(name, phoneNo));
                                directory.put(phoneNo, name);
                            }
                        }
                        pCur.close();
                    }
                }

            }
            if(cur!=null){
                cur.close();
            }
            return contactList;
        }

        @Override
        protected void onPostExecute(List<Contact> list) {
            super.onPostExecute(list);
            if(listener!=null){
                listener.onContactFetch(list);
            }
        }

        public interface OnContactFetchListener {
            void onContactFetch(List<Contact>  list);
        }
    }
}
