package dev.abhishek.imgurassist.view;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.airbnb.lottie.LottieAnimationView;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.managers.ActivitySwitchManager;

public class SplashScreenActivity extends AppCompatActivity {

    private LottieAnimationView splash_loader;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splash_loader = findViewById(R.id.splash_loader);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        splash_loader.setProgress(0);
        splash_loader.addAnimatorListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationEnd(Animator animation) {
                splash_loader.setProgress(0);
            }
        });
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new ActivitySwitchManager(SplashScreenActivity.this, MainActivity.class).openActivityWithoutSlide();
            }
        },1000);
    }
}
