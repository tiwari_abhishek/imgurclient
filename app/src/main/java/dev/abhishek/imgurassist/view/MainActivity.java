package dev.abhishek.imgurassist.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.managers.ActivitySwitchManager;
import dev.abhishek.imgurassist.managers.NotificationWorker;
import dev.abhishek.imgurassist.model.Contact;
import dev.abhishek.imgurassist.view.fragments.ContactListFragment;
import dev.abhishek.imgurassist.view.fragments.HomeFragment;
import dev.abhishek.imgurassist.view.fragments.MyUploadsFragment;
import dev.abhishek.imgurassist.view.fragments.TagsFragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    public static final String MESSAGE_STATUS = "message_status" ;
    private FloatingActionButton floatingActionButton;
    private BottomNavigationView mBottom;
    private FrameLayout frameLayout;
    private static int current_tab=0;
    private boolean permissions_granted = false;

    public static Context getAppContext() {
        return MainActivity.getAppContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setNavigationBarColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));

        checkPermissions();
        mBottom = findViewById(R.id.fab_bottom);
        frameLayout = findViewById(R.id.main_framelayout);
        openFragment(new HomeFragment(), 0);
        mBottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.fab_home:
                        floatingActionButton.setVisibility(View.VISIBLE);
                        openFragment(new HomeFragment(), 0);
                        break;

                    case R.id.action_tags:
                        openFragment(new TagsFragment(), 1);
                        floatingActionButton.setVisibility(View.GONE);
                        break;
                    case R.id.action_uploads:
                        openFragment(new MyUploadsFragment(), 2);
                        floatingActionButton.setVisibility(View.GONE);
                        break;

                    case R.id.action_contacts:
                        openFragment(new ContactListFragment(), 3);
                        floatingActionButton.setVisibility(View.GONE);
                        break;
                }
                return true;
            }
        });

        floatingActionButton = findViewById(R.id.fab1);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ActivitySwitchManager(MainActivity.this, UploadImageActivity.class).openActivityWthoutFinish();
            }
        });
    }
    public void checkPermissions(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.INTERNET,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                permissions_granted = true;
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
        MultiplePermissionsListener dialogMultiplePermissionsListener =
                DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(MainActivity.this)
                        .withTitle("Camera & audio permission")
                        .withMessage("Both camera and audio permission are needed to take pictures of your cat")
                        .withButtonText(android.R.string.ok)
                        .build();
    }
    private void openFragment(Fragment fragment, int pos) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.main_framelayout, fragment);
        fragmentTransaction.commit();
    }
}
