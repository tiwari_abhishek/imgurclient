package dev.abhishek.imgurassist.view.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.adapter.GalleryTagAdapter;
import dev.abhishek.imgurassist.managers.DatabaseHandlers;
import dev.abhishek.imgurassist.model.Contact;
import dev.abhishek.imgurassist.model.GalleryTagRoot;
import dev.abhishek.imgurassist.model.Image;
import dev.abhishek.imgurassist.model.TopPost;
import dev.abhishek.imgurassist.model.Uploads;
import dev.abhishek.imgurassist.services.ApiClient;
import dev.abhishek.imgurassist.services.GalleryTagService;
import dev.abhishek.imgurassist.services.TopPostsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private NestedScrollView tags_progress_bar;
    private Context context;
    private RecyclerView rv_home_posts;
    private DatabaseHandlers db;
    private GalleryTagAdapter adapter;
    private List<Image> imageList = new ArrayList<>();
    FragmentManager childFragmentManager ;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        childFragmentManager = getChildFragmentManager();
        context = view.getContext();
        tags_progress_bar = view.findViewById(R.id.tags_progress_bar);
        tags_progress_bar.setVisibility(View.VISIBLE);
        db = new DatabaseHandlers(view.getContext());
        rv_home_posts = view.findViewById(R.id.rv_home_posts);

        Log.d("Reading: ", "Reading all contacts..");
        List<Uploads> uploadsList = db.getAllRecords();

        for (Uploads up : uploadsList) {
            String log = "Id: " + up.getId() + " ,Name: " + up.getName() + " ,desc: " +
                    up.getDesc();
            // Writing Contacts to log
            Log.e("Name: ", log);
        }

        fetchGallery();
        return view;
    }

    private void fetchGallery(){
        TopPostsService service = ApiClient.getRetrofitInstance().create(TopPostsService.class);
        Call<GalleryTagRoot> call = service.getTopPosts();
        call.enqueue(new Callback<GalleryTagRoot>() {
            @Override
            public void onResponse(Call<GalleryTagRoot> call, Response<GalleryTagRoot> response) {
                imageList.addAll(response.body().getData().getItems());
                setUpGallery();
                adapter.notifyDataSetChanged();
                tags_progress_bar.setVisibility(View.GONE);
                Log.e("RESPONSE", "SIZE: "+response.body().getData().getItems().size());
            }

            @Override
            public void onFailure(Call<GalleryTagRoot> call, Throwable t) {
                Log.e("RESPONSE", t.getMessage()+" ");
            }
        });
    }

    private void setUpGallery(){
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, 1);
        rv_home_posts.setLayoutManager(layoutManager);
        rv_home_posts.setItemAnimator(new DefaultItemAnimator());
        adapter = new GalleryTagAdapter(context,imageList, childFragmentManager);
        rv_home_posts.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}
