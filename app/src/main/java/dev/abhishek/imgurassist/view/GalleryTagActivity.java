package dev.abhishek.imgurassist.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dev.abhishek.imgurassist.R;
import dev.abhishek.imgurassist.adapter.GalleryTagAdapter;
import dev.abhishek.imgurassist.model.GalleryTagRoot;
import dev.abhishek.imgurassist.model.Image;
import dev.abhishek.imgurassist.services.ApiClient;
import dev.abhishek.imgurassist.services.GalleryTagService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryTagActivity extends AppCompatActivity {

    private String tagname;
    private static boolean is_loading = false;
    private TextView tv_tagname;
    private List<Image> imageList = new ArrayList<>();
    private RecyclerView rv_tagGallery;
    private GalleryTagAdapter adapter;
    public static int current_page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_tag);

        tagname = getIntent().getExtras().getString("tagname");
        rv_tagGallery = findViewById(R.id.rv_tagGallery);
        tv_tagname = findViewById(R.id.tv_galleryTagname);
        tv_tagname.setText(getIntent().getExtras().getString("tagDisplayName"));
        setUpGallery();
        fetchGallery(current_page);
    }

    private void fetchGallery(int page){
        GalleryTagService service = ApiClient.getRetrofitInstance().create(GalleryTagService.class);
        Call<GalleryTagRoot> call = service.getTagGallery(tagname, "viral", "week", page);
        call.enqueue(new Callback<GalleryTagRoot>() {
            @Override
            public void onResponse(Call<GalleryTagRoot> call, Response<GalleryTagRoot> response) {
                imageList.addAll(response.body().getData().getItems());
                current_page++;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                Log.e("RESPONSE", "SIZE: "+response.body().getData().getItems().size());
            }

            @Override
            public void onFailure(Call<GalleryTagRoot> call, Throwable t) {
                Log.e("RESPONSE", t.getMessage()+" ");
            }
        });
    }
    private void setUpGallery(){
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, 1);
        rv_tagGallery.setLayoutManager(layoutManager);
        rv_tagGallery.setItemAnimator(new DefaultItemAnimator());
        adapter = new GalleryTagAdapter(this,imageList, getSupportFragmentManager());
        rv_tagGallery.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        rv_tagGallery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if(dy>0){
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int[] pastVisibleItems = layoutManager.findFirstVisibleItemPositions(null);
                    int lastVisibleItem = Math.max(pastVisibleItems[0], pastVisibleItems[1]);
                    if ( (visibleItemCount + lastVisibleItem) >= totalItemCount)
                    {
                        fetchGallery(current_page);
                    }
                }
            }
        });
    }

}
