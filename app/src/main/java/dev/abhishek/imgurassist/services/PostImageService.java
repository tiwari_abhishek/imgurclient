package dev.abhishek.imgurassist.services;

import dev.abhishek.imgurassist.model.Image;
import dev.abhishek.imgurassist.model.ImageResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface PostImageService {

    @Multipart
    @Headers({
            "Authorization: Client-ID 19924b49289ed6b",
            })
    @POST("image")
    Call<ImageResponse> postImage(
            @Query("title") String title,
            @Query("description") String description,
            @Query("album") String albumId,
            @Query("account_url") String username,
            @Part MultipartBody.Part file);

}
