package dev.abhishek.imgurassist.services;

import dev.abhishek.imgurassist.model.GalleryTagRoot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface TopPostsService {

    @Headers({
            "Authorization: Client-ID 12f190d76548be4"
    })
    @GET("gallery/t/zoom/viral/week/1")
    Call<GalleryTagRoot> getTopPosts();
}
