package dev.abhishek.imgurassist.services;

import dev.abhishek.imgurassist.model.GalleryTagRoot;
import dev.abhishek.imgurassist.model.TagsRoot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface GalleryTagService {

    @Headers({
            "Authorization: Client-ID 12f190d76548be4"
    })
    @GET("gallery/t/{tagname}/{sort}/{window}/{page}")
    Call<GalleryTagRoot> getTagGallery(
            @Path("tagname") String tagname,
            @Path("sort") String sort,
            @Path("window") String window,
            @Path("page") int page
    );
}
