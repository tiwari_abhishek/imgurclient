package dev.abhishek.imgurassist.services;


import dev.abhishek.imgurassist.model.TagsRoot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface TagsService {

    @Headers({
            "Authorization: Client-ID 12f190d76548be4"
    })
    @GET("tags")
    Call<TagsRoot> getTags();
}
